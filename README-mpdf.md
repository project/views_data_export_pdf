# mPDF Renderer README
This document describes steps and settings that are specific to the mPDF
renderer of Views Data Export PDF.

mPDF is a 100% PHP-based PDF rendering solution that can generally produce 
documents that are less than 200 pages long without requiring any special 
command-line tools.

## Requirements
The following must be installed for this renderer:
1. All of the Core Requirements described in [README.md](./README.md).
2. Composer.

## Installation and Usage
 1. (Optional, but recommended) Download, install, and enable the latest
    release of the 
    [Background Process](https://www.drupal.org/project/background_process)
    module under `sites/all/modules/contrib`.

 2. Download and unpack the latest release of this module under 
    `sites/all/modules/contrib`.

 3. From within the "modules/vde_pdf_mpdf" sub-folder of this module, run
    ```
    composer install
    ```

 4. In Drupal, enable both the "Views Data Export PDF" and 
    "Views Data Export PDF - Renderer: mPDF" modules.

 5. (Optional, but recommended) Enable the "Views Data Export PDF - Background 
    Process" module.

 6. Create a new view.

 7. Add a new "Views Data Export PDF" display to the view.

 8. Set the path of the view so that the validation error goes away. Even 
    something temporary like `admin/reports/pdf-test` should be fine.

 9. Ensure the "Format" of the display is set to "PDF file".

10. Configure PDF-specific setting of the display as desired. Settings you may
    be interested in:
    - Under "Format" > "Settings":
      - "PDF Renderer" controls how HTML content is converted into PDF format.
        If possible, use "mPDF (Background process, async)" for maximum
        reliability on large exports. Otherwise, select 
        "mPDF (In-process, blocking)".
        
      - "Provide as file"/"Force export to be saved as download" (verbiage
        varies based on whether a patch for #3112930 is applied) controls if
        the PDF generated is displayed in the browser window or whether the
        browser always downloads the file. If checked, the export is saved,
        bypassing display in the browser window.

      - "Bypass PDF rendering and export intermediate HTML instead (for
        development and styling purposes)" can be used to export the raw
        HTML output that is normally converted into PDF format, to make it
        easier for themers who are working on a custom stylesheet for the PDF.
        Do not use this setting in a production environment.

      - "Page width" and "Page height" controls the width and height,
        respectively, of a page in *portrait* orientation (taller than wide).
        If "Landscape" is checked, the dimensions are swapped accordingly.

        The default sizes are for A4. For US Letter size PDFs, use:
        - Page width:  `215.9`
        - Page height: `279.4`

      - "Custom stylesheet path" overrides the stylesheet that ships with this
        module, to give site builders/themers the ability to further customize
        how the export looks. If left empty, the default stylesheet is applied.

      - "PDF renderer"

    - Under "Data Export Settings":
      - "Batched export" is strongly recommended for all exports as it ensures
        users do not encounter timeouts on large result sets.

      - "Batch results temporary storage" should be set to "placeholder" for
        most use cases. It can be set to "full" if you are using views header 
        and footer plugins that need access to the entire result set data, or
        "none" if you are not using any views header or footer plugins.

11. Save the view.

12. Navigate to the path you set in step 8 to test out the new export.

### Adding Headers and Footers (with Page Numbers)
When using mPDF, here are the steps for adding simple page numbering to 
the _footer_ of every PDF page of an existing VDE PDF view display:

1. Open the Views Data Export PDF display of the view for editing (if you
   haven't already).

2. Add a new "Global: Text area" footer to the "Footer" region of the view.

3. (Recommended) Change "For" at the top of the modal from "All displays" to
   "This YOUR DISPLAY (override)" (e.g. "This views_data_export_pdf (override)")
   to ensure that the page numbering only affects PDF exports.

4. (Optional) Set the "Label" of the field to "Page number" to make it easier
   to find later when administering the view.

5. (Recommended) Enable "Display even if view has no result" to ensure page
   numbers always appear even when there are no results. (For other region
   handlers, enabling this option can workaround issues in Views like 
   [#1807624: Saving and rendering in empty region for 'Global: unfiltered'
   text does not work](http://drupal.org/node/1807624)).

6. (Important) Set the "Text format" to "Full HTML".

7. Enter the following HTML markup in the textarea:
   <div class="footer-right">
     Page {PAGENO} of {nbpg}
   </div>

8. Click the "Apply (this display)" button.

9. Save changes to the view.

10. Try the export.

In step 7, you can use the CSS classes `footer-left` and `footer-right` to 
designate content that is intended for the left and right sides of the footer,
respectively. 

If you are trying to create a header, in step 2 add the HTML to the "Header"
region of the view and use the CSS classes `footer-left` and `footer-right` to 
designate content that is intended for the left and right sides of the header,
respectively. 

mPDF supports replacement of various aliases/tokens in header and footer 
content, including `{PAGENO}` and `{nbpg}` referenced above. 
See [mPDF documentation](https://mpdf.github.io/what-else-can-i-do/replaceable-aliases.html)
for more information details.

## Known Issues
When using mPDF, the following are known issues:

### Timeouts when Generating Large Exports (> 100 pages)
For best results, enable batched export, with the
"mPDF (Background process, async)" renderer (requires the Background Process)
module. This should allow larger exports to run for up to 10 minutes without
issue on most hosts.

### Error about HTMl Code Size
If you get the following error:
> The HTML code size is larger than pcre.backtrack_limit XYZ. You should use
> WriteHTML() with smaller string lengths.

You can try increasing `pcre.backtrack_limit` on your instance (try doubling 
it), but that may not achieve the desired result (see below). Unfortunately,
mPDF relies heavily on regular expression parsing of HTML content, and there are
scalability limits with generating PDFs from HTML via that approach.

### Export > 500 Pages Never Completes
Even when using the background process renderer for mPDF, the export may not 
complete successfully due to the amount of HTML being export. mPDF is heavily
limited by the `memory_limit` of your PHP installation, and is not as efficient
as `wkhtmltopdf`, so it renders more slowly. It may get killed while rendering
if your host has a hard limit on how long a PHP process can be running without
a client connected to it.

Try raising the PHP `memory_limit` if you can.

Alternatively, if you are not using headers and footers in your PDF, try
the [wkhtmltopdf](./README-wkhtmltopdf.md).

If you are generating large PDFs and need both headers and footers, consider 
using the [hybrid wkhtmltopdf + mPDF Renderer](./README-wkhtmltopdf-mpdf.md).

### CSS is Applied Inconsistently or Incorrectly per the XHTML/HTML5 Spec
mPDF implements a restricted subset of CSS2/3 properties, and of the CSS 
attributes it supports, not all are supported on all elements it can render.

See [mPDF Documentation](https://mpdf.github.io/css-stylesheets/introduction.html).
