# wkhtmltopdf + mPDF Hybrid Renderer README
This document describes steps and settings that are specific to the hybrid
wkhtmltopdf and mPDF renderer of Views Data Export PDF.

The hybrid renderer attempts to overcome a limitation of wkhtmltopdf that can
prevent it from generating PDF documents that are longer than 500 pages if the
documents contain HTML headers and footers, while avoiding scalability
limitations in mPDF that prevent it from rendering long documents. This renderer
uses wkhtmltopdf to render the PDF without HTML headers and footers, and then 
uses mPDF to "stamp" headers and footers into the PDF.

## Requirements
The following must be installed for this renderer:
1. All of the Core Requirements described in [README.md](./README.md).
2. All of the Requirements described in the [wkhtmltopdf Renderer README](./README-wkhtmltopdf.md).
3. All of the Requirements described in the [mPDF Renderer README](./README-mpdf.md).

## Installation and Usage
 1. Download and install the latest release of the "PHP WK HTML to PDF" module
    under `sites/all/modules/contrib`.

 2. (Optional, but recommended) Download and unpack the latest
    release of the "Batch Process" module under `sites/all/modules/contrib`.

 3. (Optional, but recommended) Patch the "PHP WK HTML to PDF" module to
    address
    [#3012861: Fix version detection for newer versions](https://www.drupal.org/project/phpwkhtmltopdf/issues/3012861) 
    using the patch from #4 in that issue.

 4. Install wkhtmltopdf library dependencies:
    - Option #1 - Using Drush: From the root of your Drupal site, run:
      ```
      drush make --no-core -y sites/all/modules/contrib/phpwkhtmltopdf/phpwkhtmltopdf.make
      ```

    - Option #2 - Manually
      * phpwkhtmltopdf:
        - If you applied the patch in step 3, download version `2.4.1`.
        - Otherwise, download version `2.0.1` (or higher).

        The ZIP needs to be unpacked under `sites/all/libraries` such that
        `src/Pdf.php` can be found as:
        `sites/all/libraries/phpwkhtmltopdf/src/Pdf.php`

      * php-shellcommand:
        - If you applied the patch in step 3, download version `1.6.1`.
        - Otherwise, download version `1.0.3` (or higher).

        The ZIP needs to be unpacked under `sites/all/libraries` such that
        `src/Command.php` can be found as:
        `sites/all/libraries/php-shellcommand/src/Command.php`

      * php-tmpfile:
        - If you applied the patch in step 3, download version `1.1.3`.
        - Otherwise, download version `2.0.1` (or higher).

        The ZIP needs to be unpacked under `sites/all/libraries` such that
        src/File.php can be found as:
        `sites/all/libraries/php-tmpfile/src/File.php`

 5. Download and unpack the latest release of this module under 
    `sites/all/modules/contrib`.

 6. From within the "modules/vde_pdf_mpdf" sub-folder of this module, run
    ```
    composer install
    ```

 7. In Drupal, enable the following modules:
    - Views Data Export PDF
    - Views Data Export PDF - Renderer: wkhtmltopdf
    - Views Data Export PDF - Renderer: mPDF
    - Views Data Export PDF - Renderer: Hybrid wkhtmltopdf + mPDF
    - (Optional, but recommended) Views Data Export PDF - Background Process

 8. Set the path to the WkhtmlToPdf binary on the "Views data export PDF"
    settings page (/admin/config/content/views_data_export_pdf). If the binary
    is in your path, you can just enter `wkhtmltopdf` here.

 9. Create a new view.

10. Add a new "Views Data Export PDF" display to the view.

11. Set the path of the view so that the validation error goes away. Even 
    something temporary like `admin/reports/pdf-test` should be fine.

11. Ensure the "Format" of the display is set to "PDF file".

12. Configure PDF-specific setting of the display as desired. Settings you may
    be interested in:
    - Under "Format" > "Settings":
      - "PDF Renderer" controls how HTML content is converted into PDF format.
        If possible, use "wkhtmltopdf + mPDF Hybrid (Background process, async)" 
        for maximum reliability on large exports. Otherwise, select 
        "wkhtmltopdf + mPDF Hybrid (In-process, blocking)".
        
      - "Provide as file"/"Force export to be saved as download" (verbiage
        varies based on whether a patch for #3112930 is applied) controls if
        the PDF generated is displayed in the browser window or whether the
        browser always downloads the file. If checked, the export is saved,
        bypassing display in the browser window.

      - "Bypass PDF rendering and export intermediate HTML instead (for
        development and styling purposes)" can be used to export the raw
        HTML output that is normally converted into PDF format, to make it
        easier for themers who are working on a custom stylesheet for the PDF.
        Do not use this setting in a production environment.

      - "Page width" and "Page height" controls the width and height,
        respectively, of a page in *portrait* orientation (taller than wide).
        If "Landscape" is checked, the dimensions are swapped accordingly.

        The default sizes are for A4. For US Letter size PDFs, use:
        - Page width:  `215.9`
        - Page height: `279.4`

      - "Custom stylesheet path" overrides the stylesheet that ships with this
        module, to give site builders/themers the ability to further customize
        how the export looks. If left empty, the default stylesheet is applied.

      - "PDF renderer"

    - Under "Data Export Settings":
      - "Batched export" is strongly recommended for all exports as it ensures
        users do not encounter timeouts on large result sets.

      - "Batch results temporary storage" should be set to "placeholder" for
        most use cases. It can be set to "full" if you are using views header 
        and footer plugins that need access to the entire result set data, or
        "none" if you are not using any views header or footer plugins.

13. Save the view.

14. Navigate to the path you set in step 8 to test out the new export.

## Adding Headers and Footers (with Page Numbers)
mPD is used to render headers and footers.

Follow instructions for "Adding Headers and Footers (with Page Numbers)" in the
[mPDF Renderer README](./README-mpdf.md).
