# wkhtmltopdf Renderer README
This document describes steps and settings that are specific to the wkhtmltopdf
renderer of Views Data Export PDF.

`wkhtmltopdf` is a command-line tool based on WebKit that can be used to
efficiently render HTML content to PDF. Depending upon configuration, it can
generally produce documents that are thousands of pages long (though some
configurations limit it to less than 500 pages; see the "Known Issues" section
below).

## Requirements
The following must be installed for this renderer:
1. All of the Core Requirements described in [README.md](./README.md).
2. [phpwkhtmltopdf module](https://www.drupal.org/project/phpwkhtmltopdf)
3. [wkhtmltopdf CLI tool](https://wkhtmltopdf.org/)
4. PHP Libraries
   - [phpwkhtmltopdf](https://github.com/mikehaertl/phpwkhtmltopdf)
   - [php-shellcommand](https://github.com/mikehaertl/php-shellcommand)
   - [php-tmpfile](https://github.com/mikehaertl/php-tmpfile)

## Installation and Usage
 1. Download and install the latest release of the "PHP WK HTML to PDF" module
    under `sites/all/modules/contrib`.

 2. (Optional, but recommended) Download and unpack the latest
    release of the "Batch Process" module under `sites/all/modules/contrib`.

 3. (Optional, but recommended) Patch the "PHP WK HTML to PDF" module to
    address
    [#3012861: Fix version detection for newer versions](https://www.drupal.org/project/phpwkhtmltopdf/issues/3012861) 
    using the patch from #4 in that issue.

 4. Install wkhtmltopdf library dependencies:
    - Option #1 - Using Drush: From the root of your Drupal site, run:
      ```
      drush make --no-core -y sites/all/modules/contrib/phpwkhtmltopdf/phpwkhtmltopdf.make
      ```

    - Option #2 - Manually
      * phpwkhtmltopdf:
        - If you applied the patch in step 3, download version `2.4.1`.
        - Otherwise, download version `2.0.1` (or higher).

        The ZIP needs to be unpacked under `sites/all/libraries` such that
        `src/Pdf.php` can be found as:
        `sites/all/libraries/phpwkhtmltopdf/src/Pdf.php`

      * php-shellcommand:
        - If you applied the patch in step 3, download version `1.6.1`.
        - Otherwise, download version `1.0.3` (or higher).

        The ZIP needs to be unpacked under `sites/all/libraries` such that
        `src/Command.php` can be found as:
        `sites/all/libraries/php-shellcommand/src/Command.php`

      * php-tmpfile:
        - If you applied the patch in step 3, download version `1.1.3`.
        - Otherwise, download version `2.0.1` (or higher).

        The ZIP needs to be unpacked under `sites/all/libraries` such that
        src/File.php can be found as:
        `sites/all/libraries/php-tmpfile/src/File.php`

 5. Download and unpack the latest release of this module under 
    `sites/all/modules/contrib`.
    
 6. In Drupal, enable both the "Views Data Export PDF" and 
    "Views Data Export PDF - Renderer: wkhtmltopdf" modules.

 7. (Optional, but recommended) In Drupal, enable the 
    "Views Data Export PDF - Background Process" module.

 8. Set the path to the WkhtmlToPdf binary on the "Views data export PDF"
    settings page (/admin/config/content/views_data_export_pdf). If the binary
    is in your path, you can just enter `wkhtmltopdf` here.

 9. Create a new view.

10. Add a new "Views Data Export PDF" display to the view.

11. Set the path of the view so that the validation error goes away. Even 
    something temporary like `admin/reports/pdf-test` should be fine.

11. Ensure the "Format" of the display is set to "PDF file".

12. Configure PDF-specific setting of the display as desired. Settings you may
    be interested in:
    - Under "Format" > "Settings":
      - "PDF Renderer" controls how HTML content is converted into PDF format.
        If possible, use "wkhtmltopdf (Background process, async)" for maximum
        reliability on large exports. Otherwise, select 
        "wkhtmltopdf (In-process, blocking)".
        
      - "Provide as file"/"Force export to be saved as download" (verbiage
        varies based on whether a patch for #3112930 is applied) controls if
        the PDF generated is displayed in the browser window or whether the
        browser always downloads the file. If checked, the export is saved,
        bypassing display in the browser window.

      - "Bypass PDF rendering and export intermediate HTML instead (for
        development and styling purposes)" can be used to export the raw
        HTML output that is normally converted into PDF format, to make it
        easier for themers who are working on a custom stylesheet for the PDF.
        Do not use this setting in a production environment.

      - "Page width" and "Page height" controls the width and height,
        respectively, of a page in *portrait* orientation (taller than wide).
        If "Landscape" is checked, the dimensions are swapped accordingly.

        The default sizes are for A4. For US Letter size PDFs, use:
        - Page width:  `215.9`
        - Page height: `279.4`

      - "Custom stylesheet path" overrides the stylesheet that ships with this
        module, to give site builders/themers the ability to further customize
        how the export looks. If left empty, the default stylesheet is applied.

      - "PDF renderer"

    - Under "Data Export Settings":
      - "Batched export" is strongly recommended for all exports as it ensures
        users do not encounter timeouts on large result sets.

      - "Batch results temporary storage" should be set to "placeholder" for
        most use cases. It can be set to "full" if you are using views header 
        and footer plugins that need access to the entire result set data, or
        "none" if you are not using any views header or footer plugins.

13. Save the view.

14. Navigate to the path you set in step 11 to test out the new export.


## Adding Headers and Footers (with Page Numbers)
When using wkhtmltopdf, here are the steps for adding simple page numbering to 
the _footer_ of every PDF page of an existing VDE PDF view display:

1. Open the Views Data Export PDF display of the view for editing (if you
   haven't already).

2. Add a new "Global: Text area" footer to the "Footer" region of the view.

3. (Recommended) Change "For" at the top of the modal from "All displays" to
   "This YOUR DISPLAY (override)" (e.g. "This views_data_export_pdf (override)")
   to ensure that the page numbering only affects PDF exports.

4. (Optional) Set the "Label" of the field to "Page number" to make it easier
   to find later when administering the view.

5. (Recommended) Enable "Display even if view has no result" to ensure page
   numbers always appear even when there are no results. (For other region
   handlers, enabling this option can workaround issues in Views like 
   [#1807624: Saving and rendering in empty region for 'Global: unfiltered'
   text does not work](http://drupal.org/node/1807624)).

6. (Important) Set the "Text format" to "Full HTML".

7. Enter the following HTML markup in the textarea:
   <div class="footer-right">
     Page <span class="pdf-page"></span> of <span class="pdf-topage"></span>
   </div>

8. Click the "Apply (this display)" button.

9. Save changes to the view.

10. Try the export.

In step 7, you can use the CSS classes `footer-left` and `footer-right` to 
designate content that is intended for the left and right sides of the footer,
respectively. 

If you are trying to create a header, in step 2 add the HTML to the "Header"
region of the view and use the CSS classes `footer-left` and `footer-right` to 
designate content that is intended for the left and right sides of the header,
respectively. 

For wkhtmltopdf, including `pdf-page` and `pdf-topage` referenced above, here 
are all the CSS "magic" CSS classes that get replaced in headers and footers:
  * `pdf-page`       - Replaced by the number of the pages currently being 
                       printed
  * `pdf-frompage`   - Replaced by the number of the first page to be printed
  * `pdf-topage`     - Replaced by the number of the last page to be printed
  * `pdf-webpage`    - Replaced by the URL of the page being printed
  * `pdf-section`    - Replaced by the name of the current section
  * `pdf-subsection` - Replaced by the name of the current subsection
  * `pdf-date`       - Replaced by the current date in system local format
  * `pdf-isodate`    - Replaced by the current date in ISO 8601 extended format
  * `pdf-time`       - Replaced by the current time in system local format
  * `pdf-title`      - Replaced by the title of the of the current page object
  * `pdf-doctitle`   - Replaced by the title of the output document
  * `pdf-sitepage`   - Replaced by the number of the page in the current site
                       being converted
  * `pdf-sitepages`  - Replaced by the number of pages in the current site being
                       converted

These are based on the options natively supported by WK HTML to PDF (for more
information, see the "Footers and Headers" section of
https://wkhtmltopdf.org/usage/wkhtmltopdf.txt).

## Known Issues
When using wkhtmltopdf, the following are known issues:

### Timeouts when Generating Large Exports (> 200 pages)
For best results, enable batched export, with the 
"wkhtmltopdf (Background process, async)" renderer (requires the Background 
Process) module. This should allow larger exports to run for up to 10 minutes 
without issue on most hosts.

### PDFs > 500 pages Fail to Generate on Linux Hosts when Using Headers + Footers
This is caused by wkhtmlpdf having a design flaw/bug where it opens a new file
handle per header and footer per page that it is trying to render and keeps them
open until it is done generating a PDF. On Linux systems with the default 
per-process `nofiles` ulimit of `1024`, this means that the largest PDF that can
be generated with HTML headers and footers is around 500 pages.

See wkhtmltopdf 
[issue #2093](https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2093).

Unfortunately, there is nothing that Views Data Export PDF can do to workaround
this issue without losing support for formatted headers and footers. If you
eliminate either the View header or View footer in your PDF, you will be able to
generate approximately 1,000 pages instead of 500. If you eliminate both, you
should be able to generate as many pages as desired (since there is no longer a
file handle being opened per page).
 
If you are generating large PDFs, need both headers and footers, and cannot 
raise ulimits, consider using an alternate 
renderer [like the hybrid wkhtmltopdf + mPDF Renderer](./README-wkhtmltopdf-mpdf.md).
