# Views Data Export PDF

## Description
One Views Data Export PDF rendering solution to rule them all!

This module extends the
[Views Data Export](https://drupal.org/project/views_data_export) module to
support exporting tables of data to PDF format using one of several HTML-to-PDF
rendering libraries.

## Supported Renderers
- wkhtmltopdf
- mPDF
- Hybrid (wkhtmltopdf for body content, mPDF for headers and footers)

All of the above run in-process, blocking a request thread until the PDF has
generated; but, they can also run asynchronously in a background process if the
"Background Process" and "Views Data Export PDF - Background Process" modules
are installed.

## Core Requirements
The following must be installed for this renderer:
1. PHP 7.0+
2. [Views Data Export module](https://www.drupal.org/project/views_data_export)
3. [Background Process module](https://www.drupal.org/project/background_process)
   (optional, but recommended)
4. At least one renderer installed and enabled (see below).

## wkhtmltopdf Renderer
See the [wkhtmltopdf Renderer README](./README-wkhtmltopdf.md).

## mPDF Renderer
See the [mPDF Renderer README](./README-mpdf.md).

## Hybrid (wkhtmltopdf + mPDF) Renderer
See the [wkhtmltopdf + mPDF Renderer README](./README-wkhtmltopdf-mpdf.md).

Credits
-------
- [Sergey Grigorenko](https://www.drupal.org/u/svipsa)
- [Guy Elsmore-Paddock](https://www.drupal.org/u/guypaddock) at
  [Inveniem](https://inveniem.com)
