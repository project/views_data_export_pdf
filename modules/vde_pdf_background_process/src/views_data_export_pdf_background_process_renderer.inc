<?php
/**
 * @file
 * Contains the background process renderer.
 */

/**
 * A PDF renderer that wraps an in-proc PDF renderer in a Background Process.
 *
 * This implementation makes use of the Background Process API to spin-up a
 * worker sub-request on the HTTP server that is serving the current request,
 * behind any load balancers that are in place. This worker process then invokes
 * PDF conversion and reports its status whenever the batch export operation
 * requests it.
 *
 * The runtime of this approach is limited only by maximum request time limits
 * (if any) that are enforced at the backend HTTP server level or environmental
 * factors (e.g. containers going off-line or being cycled periodically by the
 * host).
 */
class views_data_export_pdf_background_process_renderer
  extends views_data_export_pdf_renderer_base {

  /**
   * The name of the module that defines the PDF renderer class.
   *
   * @var string
   */
  private $renderer_module;

  /**
   * The relative file path for the file that defines the PDF renderer class.
   *
   * @var string
   */
  private $renderer_file_path;

  /**
   * The name of the in-proc renderer used to perform the actual rendering.
   *
   * @var string
   */
  private $renderer_class_name;

  /**
   * Constructor for views_data_export_pdf_background_process_renderer.
   *
   * The new instance wraps an in-proc renderer to perform the actual rendering
   * to PDF. The module name, file name, and class name of the in-proc renderer
   * must be provided.
   *
   * @param string $renderer_module
   *   The machine name of the module that defines the PDF renderer class.
   * @param string $renderer_class_name
   *   The name of the in-proc renderer used to perform the actual rendering.
   * @param string $renderer_file_path [optional]
   *   The relative file path for the file that defines the PDF renderer class.
   *   Defaults to the class name with 'src/' prefixed and '.inc' suffixed.
   */
  public function __construct(string $renderer_module,
                              string $renderer_class_name,
                              string $renderer_file_path = NULL) {
    $this->renderer_module      = $renderer_module;
    $this->renderer_class_name  = $renderer_class_name;

    if (empty($renderer_file_path)) {
      $this->renderer_file_path =
        sprintf('src/%s.inc', $renderer_class_name);
    }
    else {
      $this->renderer_file_path = $renderer_file_path;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function render_html_file_to_pdf($source_html_file, $target_pdf_file,
                                          array $renderer_options) {
    $handle           = $this->get_lock_name($source_html_file);
    $existing_process = background_process_get_process($handle);

    // background_process_get_process() returns FALSE if the process started and
    // finished quickly, while background_process_is_started() returns FALSE if
    // the process has been created but has not yet started. So... we need both
    // to mitigate both race conditions.
    if (!empty($existing_process) || background_process_is_started($handle)) {
      $finished =
        $this->check_background_process_status($handle, $target_pdf_file);
    }
    else {
      watchdog(
        'vde_pdf_background_process',
        'Spawning background process for PDF export %handle via %renderer.',
        [
          '%handle'   => $handle,
          '%renderer' => $this->get_renderer_class_name(),
        ],
        WATCHDOG_DEBUG
      );

      $this->spawn_background_process(
        $handle,
        $source_html_file,
        $target_pdf_file,
        $renderer_options
      );

      // Process just started.
      $finished = FALSE;
    }

    return $finished;
  }

  /**
   * {@inheritDoc}
   */
  public function render_html_to_pdf(string $source_html, array $renderer_options) {
    // There's no benefit to trying to render this via a background process
    // since this method is blocking and synchronous, so we just fall back to
    // using the in-proc renderer.
    $in_proc_renderer = self::build_in_proc_renderer();

    return $in_proc_renderer->render_html_to_pdf($source_html, $renderer_options);
  }

  /**
   * Background Process callback to render the PDF.
   *
   * This invokes the underlying renderer to read-in HTML from the source file
   * and write it out to the destination file.
   *
   * @param object $source_html_file
   *   The file entity wrapping the HTML file to convert.
   * @param object $target_pdf_file
   *   The file entity wrapping an (empty) target PDF output file.
   * @param array $renderer_options
   *   The options being provided to the renderer to control display of the PDF.
   *   Can include:
   *    - orientation: 'landscape' or 'portrait'.
   *    - page_width: The width of a single portrait-orientation page, in
   *      millimeters.
   *    - page_height: The height of a single portrait-orientation page, in
   *      millimeters.
   *    - header_html: HTML markup for the header of each PDF page.
   *    - footer_html: HTML markup for the footer of each PDF page.
   *    - stylesheet_path: The path to the stylesheet that is currently applied.
   */
  public function background_process($source_html_file, $target_pdf_file,
                                     array $renderer_options) {
    watchdog(
      'vde_pdf_background_process',
      'Starting to generate PDF from %source_name to %target_name via %renderer.',
      [
        '%source_name'  => $source_html_file->uri,
        '%target_name'  => $target_pdf_file->uri,
        '%renderer'     => $this->get_renderer_class_name(),
      ],
      WATCHDOG_DEBUG
    );

    try {
      $in_proc_renderer = self::build_in_proc_renderer();

      $in_proc_renderer->render_html_file_to_pdf(
        $source_html_file,
        $target_pdf_file,
        $renderer_options
      );

      watchdog(
        'vde_pdf_background_process',
        'Successfully finished generating PDF %target_name via %renderer.',
        [
          '%target_name'  => $target_pdf_file->uri,
          '%renderer'     => $this->get_renderer_class_name(),
        ],
        WATCHDOG_DEBUG
      );
    }
    catch (Exception $ex) {
      // We're in an async background process, so this is the best we can do for
      // now.
      self::log_exception($ex);
    }
  }

  /**
   * Gets the name of the module that defines the PDF renderer class.
   *
   * @return string
   */
  public function get_renderer_module(): string {
    return $this->renderer_module;
  }

  /**
   * Gets the relative file path for the file that defines the PDF renderer
   * class.
   *
   * @return string
   */
  public function get_renderer_file_path(): string {
    return $this->renderer_file_path;
  }

  /**
   * Gets the name of the in-proc renderer used to perform the actual rendering.
   *
   * @return string
   */
  public function get_renderer_class_name(): string {
    return $this->renderer_class_name;
  }

  /**
   * Spawns a background process to perform the HTML to PDF conversion.
   *
   * @param string $handle
   *   The unique identifier to use for the background process that will process
   *   the conversion.
   * @param object $source_html_file
   *   The file entity wrapping the HTML file to convert.
   * @param object $target_pdf_file
   *   The file entity wrapping an (empty) target PDF output file.
   * @param array $renderer_options
   *   The options being provided to the renderer to control display of the PDF.
   *   Can include:
   *    - orientation: 'landscape' or 'portrait'.
   *    - page_width: The width of a single portrait-orientation page, in
   *      millimeters.
   *    - page_height: The height of a single portrait-orientation page, in
   *      millimeters.
   *    - header_html: HTML markup for the header of each PDF page.
   *    - footer_html: HTML markup for the footer of each PDF page.
   *    - stylesheet_path: The path to the stylesheet that is currently applied.
   *
   * @throws views_data_export_pdf_conversion_exception
   *   If the conversion process cannot start.
   */
  protected function spawn_background_process(string $handle, $source_html_file,
                                              $target_pdf_file,
                                              array $renderer_options) {
    $handle =
      background_process_start_locked(
        $handle,
        [$this, 'background_process'],
        $source_html_file,
        $target_pdf_file,
        $renderer_options
      );

    if (empty($handle)) {
      throw new views_data_export_pdf_conversion_exception(
        'Could not start PDF conversion. Check site logs for details.'
      );
    }
    else {
      // BUGBUG: It seems like sometimes a record of the background process
      // doesn't yet exist by the time we're polling for it in the next batch
      // request.
      //
      // Let's try giving the background process a chance to start before we
      // poll for it next. This doesn't seem like a complete/deterministic
      // solution, but should work until we can determine what's going on.
      sleep(5);
    }
  }

  /**
   * Checks to see if an existing PDF conversion process has finished.
   *
   * If the task reports that it is finished, the result of the operation is
   * examined to confirm success.
   *
   * @param string $handle
   *   The unique identifier for the background process that is processing the
   *   conversion.
   * @param object $target_pdf_file
   *   The file entity wrapping the target PDF output file.
   *
   * @return bool
   *   TRUE if the PDF was successfully converted; FALSE if the conversion
   *   process is still in process.
   *
   * @throws views_data_export_pdf_conversion_exception
   *   If the conversion finished unsuccessfully.
   */
  protected function check_background_process_status(string $handle,
                                                     $target_pdf_file) {
    // background_process_is_finished() returns TRUE if there is no progress
    // entry for the process, which can happen BEFORE the task starts. So, we
    // also need background_process_is_started() to find out if we even have a
    // progress record.
    if (background_process_is_started($handle)
        && background_process_is_finished($handle)) {
      // Reload the file to get the latest copy, so we can confirm the result.
      $target_pdf_file = entity_load_unchanged('file', $target_pdf_file->fid);

      $pdf_file_mime = $target_pdf_file->filemime ?? NULL;
      $pdf_file_size = $target_pdf_file->filesize ?? 0;

      if (($pdf_file_mime !== self::MIME_TYPE_PDF) || ($pdf_file_size == 0)) {
        throw new views_data_export_pdf_conversion_exception(
          'PDF conversion did not create the output file. Check site logs for details.'
        );
      }

      $finished = TRUE;
    } else {
      $finished = FALSE;
    }

    return $finished;
  }

  /**
   * Obtains an instance of the in-proc PDF renderer.
   *
   * The in-proc renderer performs all of the actual rendering of this renderer.
   *
   * @return views_data_export_pdf_renderer
   */
  protected function build_in_proc_renderer() {
    $file_path_without_extension =
      preg_replace('/(.+)\.inc/', '\1', $this->get_renderer_file_path());

    module_load_include(
      'inc',
      $this->get_renderer_module(),
      $file_path_without_extension
    );

    $class_name = $this->get_renderer_class_name();

    return new $class_name();
  }
}
