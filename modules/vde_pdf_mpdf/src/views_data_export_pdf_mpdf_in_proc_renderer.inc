<?php
/**
 * @file
 * Contains the class for rendering PDFs with mPDF.
 */

require_once(__DIR__ . '/../vendor/autoload.php');

use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;

/**
 * A PDF renderer that uses mPDF.
 *
 * This implementation works well on small result sets (< 100 pages of data),
 * but it's implemented 100% in PHP so it runs the risk of timing out on larger
 * result sets unless it's invoked through VDE PDF Background Process.
 */
class views_data_export_pdf_mpdf_in_proc_renderer
  extends views_data_export_pdf_renderer_base {

  /**
   * {@inheritDoc}
   */
  public function render_html_file_to_pdf($source_html_file, $target_pdf_file,
                                          array $renderer_options) {
    if (!$this->acquire_render_lock($source_html_file)) {
      return FALSE;
    }

    try {
      $mPdf       = self::build_pdf_object_from_options($renderer_options);
      $html_data  = file_get_contents($source_html_file->uri);

      if (empty($html_data)) {
        throw new views_data_export_pdf_conversion_exception(
          'No content is available from which to generate a PDF.'
        );
      }

      $mPdf->WriteHTML($html_data);
      $mPdf->Output($target_pdf_file->uri, Destination::FILE);

      $this->update_target_pdf_file($target_pdf_file);

      return TRUE;
    }
    catch (MpdfException $ex) {
      self::log_exception($ex);

      throw new views_data_export_pdf_conversion_exception(
        'Failed to convert HTML to PDF. Check site logs for details.',
        0,
        $ex
      );
    }
    finally {
      $this->release_render_lock($source_html_file);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function render_html_to_pdf(string $source_html, array $renderer_options) {
    if (empty($source_html)) {
      throw new views_data_export_pdf_conversion_exception(
        'No content is available from which to generate a PDF.'
      );
    }

    try {
      $mPdf = self::build_pdf_object_from_options($renderer_options);
      $mPdf->WriteHTML($source_html);

      /** @noinspection PhpUnnecessaryLocalVariableInspection */
      $output = $mPdf->Output('', Destination::STRING_RETURN);

      return $output;
    }
    catch (MpdfException $ex) {
      self::log_exception($ex);

      throw new views_data_export_pdf_conversion_exception(
        'Failed to convert HTML to PDF. Check site logs for details.',
        0,
        $ex
      );
    }
  }

  /**
   * Builds an mPDF API object from the given options.
   *
   * @param array $renderer_options
   *   The options being provided to the renderer to control display of the PDF.
   *   Can include:
   *    - orientation: 'landscape' or 'portrait'.
   *    - page_width: The width of a single portrait-orientation page, in
   *      millimeters.
   *    - page_height: The height of a single portrait-orientation page, in
   *      millimeters.
   *    - header_html: HTML markup for the header of each PDF page.
   *    - footer_html: HTML markup for the footer of each PDF page.
   *    - stylesheet_path: The path to the stylesheet that is currently applied.
   *
   * @return Mpdf|null
   *   An API object configured from the settings of the plug-in display.
   *
   * @throws MpdfException If mPDF cannot be initialized from the given options.
   */
  public static function build_pdf_object_from_options(array $renderer_options) {
    $mpdf_options = self::build_mpdf_options($renderer_options);

    $constructor_options = $mpdf_options['constructor']        ?? [];
    $additional_options  = $mpdf_options['additional_options'] ?? [];

    $mPdf = new Mpdf($constructor_options);

    $dpi      = $additional_options['dpi']      ?? NULL;
    $img_dpi  = $additional_options['img_dpi']  ?? NULL;

    if ($dpi !== NULL) {
      $mPdf->dpi = $dpi;
    }

    if ($img_dpi !== NULL) {
      $mPdf->img_dpi = $img_dpi;
    }

    $header_html = $additional_options['header_html'] ?? NULL;
    $footer_html = $additional_options['footer_html'] ?? NULL;

    if ($header_html !== NULL) {
      $mPdf->SetHTMLHeader($header_html);
    }

    if ($footer_html !== NULL) {
      $mPdf->SetHTMLFooter($footer_html);
    }

    drupal_alter('views_data_export_mpdf', $mPdf);

    return $mPdf;
  }

  /**
   * Builds options for mPDF appropriate for the given renderer config.
   *
   * @param array $renderer_options
   *   The options being provided to the renderer to control display of the PDF.
   *   Can include:
   *    - orientation: 'landscape' or 'portrait'.
   *    - page_width: The width of a single portrait-orientation page, in
   *      millimeters.
   *    - page_height: The height of a single portrait-orientation page, in
   *      millimeters.
   *    - header_html: HTML markup for the header of each PDF page.
   *    - footer_html: HTML markup for the footer of each PDF page.
   *    - stylesheet_path: The path to the stylesheet that is currently applied.
   *
   * @return string[][]
   *   The options to pass to mPDF.
   */
  protected static function build_mpdf_options($renderer_options) {
    $mpdf_options = [
      'constructor'         => [],
      'additional_options'  => [],
    ];

    $page_width  = $renderer_options['page_width']  ?? NULL;
    $page_height = $renderer_options['page_height'] ?? NULL;

    if (($page_width !== NULL) && ($page_height !== NULL)) {
      $format = [$page_width, $page_height];
    }
    else {
      $format = NULL;
    }

    if (($renderer_options['orientation'] ?? NULL) == 'landscape') {
      $orientation = 'L';
    }
    else {
      $orientation = 'P';
    }

    $mpdf_options['constructor'] = [
      'tempDir'       => file_directory_temp() . '/mpdf',
      'mode'          => 'utf-8',
      'format'        => $format,
      'orientation'   => $orientation,

      'margin_top'    => 18,
      'margin_right'  => 12,
      'margin_bottom' => 18,
      'margin_left'   => 12,

      'margin_header' => 9,
      'margin_footer' => 9,
    ];

    $mpdf_options['additional_options']['dpi'] =
      $renderer_options['dpi'] ?? 96;

    $mpdf_options['additional_options']['img_dpi'] =
      $renderer_options['img_dpi'] ?? 96;

    $header_html = $renderer_options['header_html'];
    $footer_html = $renderer_options['footer_html'];

    if (!empty($header_html)) {
      $mpdf_options['additional_options']['header_html'] = $header_html;
    }

    if (!empty($footer_html)) {
      $mpdf_options['additional_options']['footer_html'] = $footer_html;
    }

    drupal_alter('views_data_export_mpdf_options', $mpdf_options);

    return $mpdf_options;
  }

}
