/**
 * @file
 * Common JavaScript evaluated by WK HTML to PDF when generating headers and
 * footers.
 *
 * To maintain compatibility with WK HTML to PDF 0.12.0, the code in this file
 * must be compatible with QtWeb 3.8.4, which uses AppleWebKit 534.34, an
 * ancient version of WebKit from 2011 that is roughly equivalent to Apple
 * Safari 5.1 (which used WebKit 534.48.3).
 */

/**
 * Replaces all placeholders in header or footer markup with appropriate context
 * from WK HTML to PDF.
 *
 * Example of placeholder markup:
 * Page <span class="pdf-page"></span> of <span class="pdf-topage"></span>
 *
 * Becomes the following for the first page of a ten page PDF:
 * Page 1 of 10
 */
function replacePlaceholders() {
  const placeholderNames = [
    'page',
    'frompage',
    'topage',
    'webpage',
    'section',
    'subsection',
    'date',
    'isodate',
    'time',
    'title',
    'doctitle',
    'sitepage',
    'sitepages'
  ];

  var pdfVars = {};
  var qsVars = window.location.search.substring(1).split('&');

  qsVars.forEach(function (qsVar) {
    const qsVarParts = qsVar.split('=', 2);
    const qsKey = qsVarParts[0];
    const rawQsValue = qsVarParts[1];

    pdfVars[qsKey] = unescape(rawQsValue);
  });

  placeholderNames.forEach(function (placeholderName) {
    const elements = document.getElementsByClassName("pdf-" + placeholderName);

    for (var elementIdx = 0; elementIdx < elements.length; ++elementIdx) {
      const element = elements[elementIdx];

      if (pdfVars[placeholderName]) {
        element.textContent = pdfVars[placeholderName];
      }
      else {
        // Not available -- possibly we're running on an older version that
        // doesn't support the variable.
        element.textContent = '';
      }
    }
  });
}

window.onload = replacePlaceholders;
