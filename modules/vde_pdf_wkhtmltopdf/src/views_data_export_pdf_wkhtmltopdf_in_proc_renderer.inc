<?php
/**
 * @file
 * Contains the class for rendering PDFs with WK HTML to PDF.
 */

/**
 * A PDF renderer that invokes WK HTML to PDF via an in-process shell call.
 *
 * This implementation works reasonably well on small to medium result sets
 * (< 200 pages of data), but runs the risk of timing out on larger result sets
 * unless it's invoked through VDE PDF Background Process.
 *
 * Use caution on exports > 500 pages (even if run inside a background process)
 * because of a documented, known issue with open file limits. Systems that
 * have a ulimit of 1024 open files per process will be limited to 500 page
 * exports.
 *
 * See:
 * https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2093
 */
class views_data_export_pdf_wkhtmltopdf_in_proc_renderer
  extends views_data_export_pdf_renderer_base {

  /**
   * {@inheritDoc}
   */
  public function render_html_file_to_pdf($source_html_file, $target_pdf_file,
                                          array $renderer_options) {
    if (!$this->acquire_render_lock($source_html_file)) {
      return FALSE;
    }

    try {
      $html_data = file_get_contents($source_html_file->uri);
      $pdf_data  = $this->render_html_to_pdf($html_data, $renderer_options);

      file_put_contents($target_pdf_file->uri, $pdf_data);
      $this->update_target_pdf_file($target_pdf_file);

      return TRUE;
    }
    finally {
      $this->release_render_lock($source_html_file);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function render_html_to_pdf(string $source_html,
                                     array $renderer_options) {
    if (empty($source_html)) {
      throw new views_data_export_pdf_conversion_exception(
        'No content is available from which to generate a PDF.'
      );
    }

    $pdf = self::build_pdf_object_from_options($renderer_options);
    $pdf->addPage($source_html, [], WkHtmlToPdf::TYPE_HTML);
    $pdf_data = $pdf->toString();

    if (empty($pdf_data)) {
      self::log_error(
        'Failed to convert HTML to PDF: <pre>@message</pre>',
        [ '@message' => $pdf->getError() ]
      );

      throw new views_data_export_pdf_conversion_exception(
        'Failed to convert HTML to PDF. Check site logs for details.'
      );
    }
    else {
      return $pdf_data;
    }
  }

  /**
   * Builds a WK HTML to PDF API object from the given options.
   *
   * @param array $options
   *   The options being provided by the Views PDF Export display plug-in.
   *
   * @return \WkHtmlToPdf|null
   *   An API object configured from the settings of the plug-in display; or
   *   NULL if the PHP WK HTML to PDF library cannot be loaded.
   */
  public function build_pdf_object_from_options(array $options) {
    if (!self::load_wkhtmltopdf_library()) {
      return NULL;
    }

    $wkhtmltopdf_options = self::build_wkhtmltopdf_options($options);

    return new \WkHtmlToPdf($wkhtmltopdf_options);
  }

  /**
   * Attempts to load the WK HTML to PDF library.
   *
   * If the library cannot be loaded, an appropriate error message is logged to
   * site logs.
   *
   * @return bool
   *   TRUE if the library loaded successfully; or, FALSE if it failed to load.
   */
  protected static function load_wkhtmltopdf_library() {
    $library = libraries_load('phpwkhtmltopdf');

    if (empty($library['loaded'])) {
      $library_error  = $library['error message'] ?? 'no error returned';
      $error          = 'Failed to load library: @message';

      self::log_error($error, ['@message' => $library_error]);

      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Builds options for WK HTML to PDF appropriate for the given renderer
   * config.
   *
   * @param array $renderer_options
   *   The options being provided to the renderer to control display of the PDF.
   *   Can include:
   *    - orientation: 'landscape' or 'portrait'.
   *    - page_width: The width of a single portrait-orientation page, in
   *      millimeters.
   *    - page_height: The height of a single portrait-orientation page, in
   *      millimeters.
   *    - header_html: HTML markup for the header of each PDF page.
   *    - footer_html: HTML markup for the footer of each PDF page.
   *    - stylesheet_path: The path to the stylesheet that is currently applied.
   *
   * @return string[]
   *   The command-line options to pass to WK HTML to PDF.
   */
  protected static function build_wkhtmltopdf_options($renderer_options) {
    $wkhtmltopdf_options = [
      'binary'      => vde_pdf_wkhtmltopdf_get_wkhtmltopdf_path(),
      'orientation' => $renderer_options['orientation'],
    ];

    $page_width  = $renderer_options['page_width']  ?? '';
    $page_height = $renderer_options['page_height'] ?? '';

    if (!empty($page_width) && !empty($page_height)) {
      $wkhtmltopdf_options['page-width']  = $page_width;
      $wkhtmltopdf_options['page-height'] = $page_height;
    }

    // Do not fail to render the entire PDF if a resource fails to load.
    $wkhtmltopdf_options['load-error-handling']       = 'ignore';
    $wkhtmltopdf_options['load-media-error-handling'] = 'ignore';

    $header_html = $renderer_options['header_html'] ?? NULL;
    $footer_html = $renderer_options['footer_html'] ?? NULL;

    if (!empty($header_html)) {
      $wkhtmltopdf_options['header-html'] = $header_html;
    }

    if (!empty($footer_html)) {
      $wkhtmltopdf_options['footer-html'] = $footer_html;
    }

    $margin_types = [
      'margin_top'    => 'margin-top',
      'margin_right'  => 'margin-right',
      'margin_bottom' => 'margin-bottom',
      'margin_left'   => 'margin-left',
    ];

    // Allow margins to be passed-in
    foreach ($margin_types as $plugin_option => $cli_option) {
      $margin_value = $renderer_options[$plugin_option] ?? NULL;

      if ($margin_value !== NULL) {
        $wkhtmltopdf_options[$cli_option] = $margin_value;
      }
    }

    drupal_alter('views_data_export_wkhtmltopdf_options', $wkhtmltopdf_options);

    return $wkhtmltopdf_options;
  }

}
