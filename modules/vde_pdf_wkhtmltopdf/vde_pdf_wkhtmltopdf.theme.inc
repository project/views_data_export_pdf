<?php
/**
 * @file
 * Theme and preprocess functions for "Views Data Export PDF - Renderer:
 * wkhtmltopdf".
 */

/**
 * Preprocess views_data_export_pdf_pheader.
 */
function vde_pdf_wkhtmltopdf_preprocess_views_data_export_pdf_pheader(array &$vars) {
  $render_engine = $vars['render_engine'] ?? NULL;
  $renderer      = $vars['renderer']      ?? NULL;

  // Special case: Ignore the Hybrid wkhtmltopdf + mPDF renderer, since it
  // does not render footers through wkhtmltopdf
  if (($render_engine == 'wkhtmltopdf')
      && (strpos($renderer, 'mpdf') === FALSE)) {
    $vars['script_path'] = vde_pdf_wkhtmltopdf_get_header_footer_js_path();

    _vd_pdf_wkhtmltopdf_inline_css_and_js($vars);
  }
}

/**
 * Preprocess views_data_export_pdf_pfooter.
 */
function vde_pdf_wkhtmltopdf_preprocess_views_data_export_pdf_pfooter(array &$vars) {
  $render_engine = $vars['render_engine'] ?? NULL;
  $renderer      = $vars['renderer']      ?? NULL;

  // Special case: Ignore the Hybrid wkhtmltopdf + mPDF renderer, since it
  // does not render footers through wkhtmltopdf
  if (($render_engine == 'wkhtmltopdf')
      && (strpos($renderer, 'mpdf') === FALSE)) {
    $vars['script_path'] = vde_pdf_wkhtmltopdf_get_header_footer_js_path();

    _vd_pdf_wkhtmltopdf_inline_css_and_js($vars);
  }
}

/**
 * In-lines the header stylesheets and scripts in HTML <head> tag.
 *
 * Workaround for https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2083.
 * In-line stylesheets and scripts avoid the header/footer being captured before
 * it has fully rendered.
 *
 * @param array $vars
 *   A reference to the theme template variables being passed to the header or
 *   footer. They will be modified by this method.
 */
function _vd_pdf_wkhtmltopdf_inline_css_and_js(array &$vars) {
  $stylesheet_path = $vars['stylesheet_path'] ?? NULL;
  $script_path     = $vars['script_path']     ?? NULL;

  if (!empty($stylesheet_path)) {
    $vars['stylesheet_body'] = file_get_contents($stylesheet_path);
  }

  if (!empty($script_path)) {
    $vars['script_body'] = file_get_contents($script_path);
  }
}
