<?php
/**
 * @file
 * Contains the PDF conversion exception class.
 */
class views_data_export_pdf_conversion_exception
extends Exception {
  /**
   * Constructor for views_data_export_pdf_conversion_exception.
   *
   * @param string $message [optional]
   *   The Exception message to throw.
   * @param int $code [optional]
   *   The Exception code.
   * @param Throwable $previous [optional]
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(string $message = "PDF conversion failed.",
                              int $code = 0,
                              Throwable $previous = null) {
    parent::__construct($message, $code, $previous);
  }
}
