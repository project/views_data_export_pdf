<?php
/**
 * @file
 * Contains a trait for simplifying logging of errors and exceptions.
 */

/**
 * A trait that provides some built-in methods for logging error messages.
 */
trait views_data_export_pdf_error_reporting_trait {
  /**
   * Logs a warning message for this module to site logs.
   *
   * @param string $message
   *   The error message to log.
   * @param array|null $variables
   *   Array of variables to replace in the message on display; or, NULL if
   *   the message is already translated or not possible to translate.
   */
  protected static function log_warning(string $message,
                                        array $variables = []) {
    watchdog('views_data_export_pdf', $message, $variables, WATCHDOG_WARNING);
  }

  /**
   * Logs an error message for this module to site logs.
   *
   * @param string $message
   *   The error message to log.
   * @param array|null $variables
   *   Array of variables to replace in the message on display; or, NULL if
   *   the message is already translated or not possible to translate.
   */
  protected static function log_error(string $message, array $variables = []) {
    watchdog('views_data_export_pdf', $message, $variables);
  }

  /**
   * Logs an exception to site logs.
   *
   * @param Exception $ex
   *   The exception to log.
   */
  protected static function log_exception(\Exception $ex) {
    watchdog_exception('views_data_export_pdf', $ex);
  }
}
