<?php
/**
 * @file
 * Contains the interface for PDF rendering.
 */

/**
 * An interface for invoking WK HTML to PDF to render an HTML document to PDF.
 */
interface views_data_export_pdf_renderer {
  /**
   * The MIME type for the final PDF.
   */
  const MIME_TYPE_PDF = 'application/pdf';

  /**
   * The MIME type for the source HTML data.
   */
  const MIME_TYPE_HTML = 'text/html';

  /**
   * Converts the contents of the given HTML file into PDF format.
   *
   * @param object $source_html_file
   *   The file entity wrapping the HTML file to convert.
   * @param object $target_pdf_file
   *   The file entity wrapping an (empty) target PDF output file.
   * @param array $renderer_options
   *   The options being provided to the renderer to control display of the PDF.
   *   Can include:
   *    - orientation: 'landscape' or 'portrait'.
   *    - page_width: The width of a single portrait-orientation page, in
   *      millimeters.
   *    - page_height: The height of a single portrait-orientation page, in
   *      millimeters.
   *    - header_html: HTML markup for the header of each PDF page.
   *    - footer_html: HTML markup for the footer of each PDF page.
   *    - stylesheet_path: The path to the stylesheet that is currently applied.
   *
   * @return bool
   *   TRUE if conversion of the file was attempted and has completed; FALSE if
   *   the conversion is still in progress.
   *
   * @throws views_data_export_pdf_conversion_exception
   *   If the conversion fails.
   */
  public function render_html_file_to_pdf($source_html_file, $target_pdf_file,
                                          array $renderer_options);

  /**
   * Converts the given HTML content into PDF format.
   *
   * Since the PDF output must be returned by this function, the conversion is
   * performed synchronously, and therefore blocks further request processing
   * until the conversion is complete.
   *
   * @param string $source_html
   *   The HTML content to convert.
   * @param array $renderer_options
   *   The options being provided to the renderer to control display of the PDF.
   *   Can include:
   *    - orientation: 'landscape' or 'portrait'.
   *    - page_width: The width of a single portrait-orientation page, in
   *      millimeters.
   *    - page_height: The height of a single portrait-orientation page, in
   *      millimeters.
   *    - header_html: HTML markup for the header of each PDF page.
   *    - footer_html: HTML markup for the footer of each PDF page.
   *    - stylesheet_path: The path to the stylesheet that is currently applied.
   *
   * @return string
   *   The result of the conversion.
   *
   * @throws views_data_export_pdf_conversion_exception
   *   If the conversion fails.
   */
  public function render_html_to_pdf(string $source_html,
                                     array $renderer_options);
}
