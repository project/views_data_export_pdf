<?php
/**
 * @file
 * Contains the base class for PDF renderers.
 */

/**
 * Abstract base class for PDF renderers.
 *
 * Contains several pieces of reusable functionality.
 */
abstract class views_data_export_pdf_renderer_base
  implements views_data_export_pdf_renderer {

  use views_data_export_pdf_error_reporting_trait;

  /**
   * The maximum amount of time in seconds that a request thread can be
   * converting the same HTML file to PDF before another request can try again.
   *
   * This is 10 minutes.
   */
  const PDF_GENERATION_LOCK_TIMEOUT = 600;

  /**
   * Gets the lock handle to use for exports from the specified file.
   *
   * @param object $input_file
   *   The input file entity.
   *
   * @return string
   *   The name of the lock/handle to use for the given file.
   */
  protected function get_lock_name($input_file) {
    return 'views_data_export_pdf_generation:' . $input_file->fid;
  }

  /**
   * Attempts to acquire a lock for exports from the specified file.
   *
   * @param object $input_file
   *   The input file entity.
   *
   * @return bool
   *   TRUE if the lock was acquired; or, FALSE otherwise.
   */
  protected function acquire_render_lock($input_file) {
    $lock_name = $this->get_lock_name($input_file);

    return lock_acquire($lock_name, self::PDF_GENERATION_LOCK_TIMEOUT);
  }

  /**
   * Attempts to release a lock for exports on the specified file.
   *
   * @param object $input_file
   *   The input file entity.
   */
  protected function release_render_lock($input_file) {
    $lock_name = $this->get_lock_name($input_file);

    lock_release($lock_name);
  }

  /**
   * Updates the MIME type and file size of the output, target PDF file.
   *
   * @param object $target_pdf_file
   *   The Drupal file entity wrapping the PDF file that was rendered.
   */
  protected function update_target_pdf_file($target_pdf_file) {
    $target_pdf_file->filemime = self::MIME_TYPE_PDF;
    $target_pdf_file->filesize = filesize($target_pdf_file->uri);

    file_save($target_pdf_file);
  }
}
