<?php
/**
 * @file
 * Contains the View Result Temp Store class.
 */

/**
 * A class for reading and writing view results from a temporary file.
 */
class views_data_export_pdf_view_full_result_file_store
  implements views_data_export_pdf_view_result_temp_store {
  /**
   * The view for which results are being stashed.
   *
   * @var view
   */
  private $view;

  /**
   * A reference to the batch export sandbox.
   *
   * @var array
   */
  private $sandbox;

  /**
   * A callable that can be invoked to create temporary files.
   *
   * @var callable
   */
  private $temp_file_factory_func;

  /**
   * Constructor for views_result_temp_store.
   *
   * @param view $view
   *   The view for which results are being stashed.
   * @param array $sandbox
   *   A reference to the batch export sandbox.
   * @param callable $temp_file_factory_func
   *   A callable that can be invoked to create temporary files. The callable
   *   must return the unique ID of the temporary file entity.
   */
  public function __construct(view $view, array &$sandbox,
                              callable $temp_file_factory_func) {
    $this->view = $view;
    $this->sandbox =& $sandbox;
    $this->temp_file_factory_func = $temp_file_factory_func;
  }

  /**
   * {@inheritDoc}
   */
  public function stash_view_results() {
    $saved_results = $this->load_view_results();
    $new_results = array_merge($saved_results, $this->view->result ?? []);

    $this->save_view_results($new_results);
  }

  /**
   * {@inheritDoc}
   */
  public function restore_view_results() {
    $view_result_file = $this->load_or_create_view_results_file();
    $saved_view_results = $this->unpack_view_results($view_result_file);

    $this->view->result = $saved_view_results;
  }

  /**
   * {@inheritDoc}
   */
  public function cleanup() {
    if (!isset($this->sandbox['view_result_fid'])) {
      return;
    }

    $view_result_file = $this->load_or_create_view_results_file();

    if (!empty($view_result_file_path) && is_file($view_result_file_path)) {
      file_delete($view_result_file);
    }

    unset($this->sandbox['view_result_fid']);
  }

  /**
   * Loads the results that have been stashed away by this temp store.
   *
   * @return array
   *   The unpacked, deserialized view results in the same format as
   *   $this->view-results.
   */
  protected function load_view_results() {
    $view_result_file = $this->load_or_create_view_results_file();
    $view_result_file_path = $view_result_file->uri ?? FALSE;

    if (empty($view_result_file_path) || !is_file($view_result_file_path)) {
      // Failed to load.
      $view_results = [];
    }
    else {
      $view_results = $this->unpack_view_results($view_result_file);
    }

    return $view_results;
  }

  /**
   * Saves new view results to the temp store.
   *
   * The data is serialized before being written out to a temporary file.
   *
   * @param array $view_results
   */
  protected function save_view_results(array $view_results) {
    $view_result_file = $this->load_or_create_view_results_file();
    $view_result_file_path = $view_result_file->uri ?? FALSE;

    if (!empty($view_result_file_path) && is_file($view_result_file_path)) {
      $view_result_data = serialize($view_results);

      file_put_contents($view_result_file_path, $view_result_data);
    }
  }

  /**
   * Deserializes and returns view results from a temporary file.
   *
   * @param object $view_result_file
   *   The temporary file entity loaded via load_or_create_view_results_file().
   *
   * @return array|FALSE
   *   Either the deserialized view results; or FALSE if the file could not be
   *   loaded.
   */
  protected function unpack_view_results($view_result_file) {
    $view_result_file_path = $view_result_file->uri ?? FALSE;

    if (empty($view_result_file_path)) {
      // Failed to load.
      return FALSE;
    }

    $view_result_data = file_get_contents($view_result_file_path);

    if (empty($view_result_data)) {
      $view_results = [];
    }
    else {
      $view_results = unserialize($view_result_data);
    }

    return $view_results;
  }

  /**
   * Loads or creates a temporary file for stashing batched view results.
   *
   * We create a temporary file to store the views results rather than storing
   * them directly in the batch sandbox so that we don't spam the InnoDB redo
   * log with blob updates.
   *
   * @return object|FALSE
   *   The Drupal file entity for the temporary file; or FALSE if the file
   *   cannot be loaded.
   */
  protected function load_or_create_view_results_file() {
    if (!isset($this->sandbox['view_result_fid'])) {
      $temp_file_factory = $this->temp_file_factory_func;
      $this->sandbox['view_result_fid'] = $temp_file_factory();
    }

    $fid = $this->sandbox['view_result_fid'];
    $file = file_load($fid);

    if ($file === FALSE) {
      watchdog(
        'views_data_export_pdf',
        'Failed to load the view result temporary file %file. Views plugins '.
        'that require the entire result set (e.g. header and footer plugins) '.
        'may produce inconsistent results.',
        [ '%file' => $fid ]
      );
    }

    return $file;
  }
}
