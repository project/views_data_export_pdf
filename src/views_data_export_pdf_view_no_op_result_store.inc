<?php
/**
 * @file
 * Contains the no-op View Result Temp Store implementation.
 */

/**
 * A no-op view result storage implementation.
 *
 * This implementation doesn't store anything at all.
 */
class views_data_export_pdf_view_no_op_result_store
  implements views_data_export_pdf_view_result_temp_store {
  /**
   * Constructor for views_data_export_view_result_no_op_store.
   */
  public function __construct() {
  }

  /**
   * {@inheritDoc}
   */
  public function stash_view_results() {
    // No op
  }

  /**
   * {@inheritDoc}
   */
  public function restore_view_results() {
    // No op
  }

  /**
   * {@inheritDoc}
   */
  public function cleanup() {
    // No op
  }
}
