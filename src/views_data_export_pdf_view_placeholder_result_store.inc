<?php
/**
 * @file
 * Contains the "Placeholder" View Result Temp Store class.
 */

/**
 * A class that only stores and restores stand-in values for view results.
 *
 * This store creates stand-ins for each view result, such that the *count* of
 * results is accurate for headers and footer result summaries, *without any
 * actual view result data*. This should work for most built-in Views header and
 * footer plugins.
 */
class views_data_export_pdf_view_placeholder_result_store
  implements views_data_export_pdf_view_result_temp_store {
  /**
   * The view for which results are being stashed.
   *
   * @var view
   */
  private $view;

  /**
   * A reference to the batch export sandbox.
   *
   * @var array
   */
  private $sandbox;

  /**
   * Constructor for views_data_export_view_result_placeholder_store.
   *
   * @param view $view
   *   The view for which results are being stashed.
   * @param array $sandbox
   *   A reference to the batch export sandbox.
   */
  public function __construct(view $view, array &$sandbox) {
    $this->view = $view;
    $this->sandbox =& $sandbox;
  }

  /**
   * {@inheritDoc}
   */
  public function stash_view_results() {
    $view_result_count = $this->sandbox['view_result_count'] ?? 0;
    $view_result_count += count($this->view->result);

    $this->sandbox['view_result_count'] = $view_result_count;
  }

  /**
   * {@inheritDoc}
   */
  public function restore_view_results() {
    $view_result_count = $this->sandbox['view_result_count'] ?? 0;

    $this->view->result =
      array_fill(0, $view_result_count, [ '_fake_placeholder' => TRUE ]);
  }

  /**
   * {@inheritDoc}
   */
  public function cleanup() {
    unset($this->sandbox['view_result_count']);
  }
}
