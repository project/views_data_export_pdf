<?php
/**
 * @file
 * Contains the interface for View Result Stores.
 */

/**
 * A class for reading and writing view results from a temporary store.
 */
interface views_data_export_pdf_view_result_temp_store {
  /**
   * Saves view results during a batched export to the temporary store.
   *
   * The current results are appended to any existing view results already in
   * the sandbox. When coupled with a call to restore_view_results() at the end
   * of the export, this can be used to allow Views plugins like header and
   * footer handlers to access the entire result set the same way that they
   * would if the export was not batched.
   */
  public function stash_view_results();

  /**
   * Restores stashed view results from the temporary store.
   *
   * The current results of the view are replaced with those stashed in the
   * sandbox. After this method has been called, all of the results of the batch
   * operation that have been previously stashed in the sandbox are available.
   * This allows Views plugins like header and footer handlers to access the
   * result set the same way that they would if the export was not batched.
   */
  public function restore_view_results();

  /**
   * Deletes any temporary data allocated by the temporary store.
   */
  public function cleanup();
}
