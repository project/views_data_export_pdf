<?php
/**
 * @file
 * Theme export PDF header.
 *
 * Variables:
 *   - view: The view being exported.
 *   - head: Header mark-up to include within the <head> tag (typically
 *     stylesheets).
 *   - options: The current settings for the PDF style plugin being used by the
 *     view for the export.
 */
?>
<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <?php if (!empty($head)): ?>
    <?php print $head; ?>
  <?php endif ?>
</head>
<body>
