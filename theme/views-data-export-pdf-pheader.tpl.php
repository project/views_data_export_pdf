<?php
/**
 * @file
 * Theme export page header.
 *
 * Available variables:
 *   - view: The view being exported.
 *   - options: The configuration options of the PDF export Views style plugin.
 *   - renderer: The machine name of the PDF renderer currently in use.
 *     (May not match "pdf_renderer" in "options" if the chosen renderer is not
 *     available).
 *   - render_engine: The machine name of the type of engine the renderer is
 *     using (e.g. "wkhtmltopdf", "mpdf", etc).
 *   - stylesheet_path: The relative URL to the stylesheet to apply when
 *     rendering the header.
 *   - script_path: The relative URL to the script to evaluate when rendering the
 *     header.
 */
$header_content = $view->display_handler->render_header();

// Workaround for https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2684
// Prevents a segmentation fault when the header has nothing in the body
// region on WK HTML to PDF version 0.12.0 (which happens to be the version
// Pantheon is still using).
//
// Note that removing this code may not necessarily trigger the issue since we
// now have <div>s in the body region, but that's not guaranteed to work 100%
// deterministically, while skipping all rendering of the header when it's
// empty is.
if (empty(trim($header_content))) {
  return '';
}
?>
<!doctype html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <?php if (!empty($stylesheet_body)): ?>
    <style>
      <?php print $stylesheet_body; ?>
    </style>
  <?php endif; ?>
  <?php if (!empty($script_body)): ?>
    <script>
      <?php print $script_body; ?>
    </script>
  <?php endif; ?>
</head>
<body class="pdf-page-header">
  <div class="pdf-page-header-content--<?php print $render_engine; ?>">
    <?php print $header_content; ?>
  </div>
</body>
</html>
