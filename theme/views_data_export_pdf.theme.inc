<?php
/**
 * @file
 * Theme and preprocess functions for "Views Data Export PDF".
 */

/**
 * Implements template_preprocess_views_data_export_pdf_body().
 */
function template_preprocess_views_data_export_pdf_body(&$vars) {
  module_load_include("inc", "views_data_export", "theme/views_data_export.theme");
  _views_data_export_header_shared_preprocess($vars);
  _views_data_export_body_shared_preprocess($vars);
}

/**
 * Implements hook_process_views_data_export_pdf_body().
 */
function template_process_views_data_export_pdf_body(&$vars) {
  $output = '';
  static $table_index = 0;

  $table_index++;
  $already_added_group_titles = (isset($vars['group_titles'])) ? $vars['group_titles'] : [];

  $flip = [
    'even' => 'odd',
    'odd' => 'even',
  ];

  $class = 'even';

  // Add grouping.
  if (!empty($vars['title']) && !in_array($vars['title'], $already_added_group_titles)) {
    $_already_added_groups[] = $vars['title'];

    if ($vars['options']['each_group_separate_table']) {
      $output .= '<div class="group-title"><strong>' . $vars['title'] . '</strong></div>';
    }
    else {
      $cell = [
        'data' => '<div class="group-title"><strong>' . $vars['title'] . '</strong></div>',
        'colspan' => count($vars['header']),
      ];
      $output .= ' <tr>';
      $output .= _theme_table_cell($cell);
      $output .= " </tr>\n";
    }

    if ($vars['options']['each_group_separate_table']) {
      if ($table_index > 1) {
        $output .= theme("views_data_export_pdf_tfooter", $vars);
      }
      $output .= theme("views_data_export_pdf_theader", $vars);
    }
  }

  foreach ($vars['themed_rows'] as $number => $cells) {
    $attributes = [];
    if (count($cells)) {
      // Add odd/even class.
      $class = $flip[$class];
      if (isset($attributes['class'])) {
        $attributes['class'] .= ' ' . $class;
      }
      else {
        $attributes['class'] = $class;
      }

      // Build row.
      $output .= ' <tr' . drupal_attributes($attributes) . '>';

      foreach ($cells as $field_name => $cell) {
        $table_cell = [ 'data'  => $cell ];

        if (!empty($field_name)) {
          $class_name =
            drupal_clean_css_identifier(
              sprintf("table-field-cell-%s", strtolower($field_name))
            );

          $table_cell['class'] = [
            'table-field-cell',
            $class_name,
          ];
        }

        $output .= _theme_table_cell($table_cell);
      }

      $output .= " </tr>\n";
    }
  }

  $vars['tbody'] = $output;
}

/**
 * Preprocess views_data_export_pdf_theader.
 */
function template_preprocess_views_data_export_pdf_theader(&$vars) {
  $vars['theme_hook_suggestions'][] = "views_data_export_pdf_theader__" . $vars['view']->name;

  _views_data_export_header_shared_preprocess($vars);

  // Need to do a little work to construct the table header, see theme_table().
  $vars['header_row'] = '';
  $vars['header_row'] .= '<thead><tr>';

  foreach ($vars['header'] as $field_name => $cell) {
    $table_cell = [
      'data'  => $cell,
      'class' => [
        'table-field-header',
        'table-column-header',
      ]
    ];

    if (!empty($field_name)) {
      $field_class_name =
        drupal_clean_css_identifier(
          sprintf("table-field-header--%s", strtolower($field_name))
        );

      $table_cell['class'][] = $field_class_name;
    }

    if (!empty($cell)) {
      $column_class_name =
        drupal_clean_css_identifier(
          sprintf("table-column-header--%s", strtolower($cell))
        );

      $table_cell['class'][] = $column_class_name;
    }

    $vars['header_row'] .= _theme_table_cell($table_cell, TRUE);
  }

  $vars['header_row'] .= '</tr></thead>';
}

/**
 * Preprocess views_data_export_pdf_tfooter.
 */
function template_preprocess_views_data_export_pdf_tfooter(&$vars) {
  $vars['theme_hook_suggestions'][] = "views_data_export_pdf_tfooter__" . $vars['view']->name;
}

/**
 * Preprocess views_data_export_pdf_pheader.
 */
function template_preprocess_views_data_export_pdf_pheader(&$vars) {
  $vars['theme_hook_suggestions'][] = "views_data_export_pdf_pheader__" . $vars['view']->name;
}

/**
 * Preprocess views_data_export_pdf_pfooter.
 */
function template_preprocess_views_data_export_pdf_pfooter(&$vars) {
  $vars['theme_hook_suggestions'][] = "views_data_export_pdf_pfooter__" . $vars['view']->name;
}

/**
 * Preprocess views_data_export_pdf.
 */
function template_preprocess_views_data_export_pdf(&$vars) {
  $vars['header'] = $vars['rows']['header'];
  $vars['body'] = $vars['rows']['body'];
  $vars['footer'] = $vars['rows']['footer'];
}
