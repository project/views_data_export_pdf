<?php

/**
 * @file
 * Views_data_export_pdf admin form.
 */

/**
 * Implements form callback.
 */
function views_data_export_pdf_settings_form() {
  $form = array();

  // Empty by default
  $form['empty_note'] = [
    '#type'   => 'markup',
    '#markup' =>
      '<p>' .
      t('This page provides a central place to configure global settings for Views Data Export PDF renderers.') .
      '</p><p>' .
      t('If this form is empty, then no installed renderers require global configuration.') .
      '</p>',
  ];

  return system_settings_form($form);
}
