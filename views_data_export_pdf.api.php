<?php

/**
 * @file
 * API documentation for Views Data Export PDF.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defines PDF renderers for converting HTML to PDF.
 *
 * This hook allows modules to register additional PDF renderers that can be
 * used when performing an export.
 *
 * @return array[]
 *   An array of renderers. Each renderer has a key corresponding to a unique
 *   machine name that is used elsewhere in the VDE PDF UI to identify the
 *   renderer; it must be unique across all modules the define renderers. The
 *   corresponding array value is an associative array that may contain the
 *   following key-value pairs:
 *   - "title": Required. The translated, human-friendly name of the renderer.
 *   - "description": The translated description of the renderer.
 *   - "class": Required. The fully-qualified name of the renderer class to
 *     instantiate.
 *   - "file": A file that will be included before the renderer is instantiated.
 *     The file should be relative to the implementing module's directory unless
 *     otherwise specified by the "file path" option.
 *   - "file path": The path to the directory containing the file specified in
 *     "file". This defaults to the path to the module implementing the hook.
 *   - "constructor arguments": An array containing the arguments to pass to the
 *     renderer class upon instantiation.
 *
 * @ingroup views_data_export_pdf
 */
function hook_pdf_export_renderers() {
  $renderers = [];

  $renderers['wkhtmltopdf_in_proc'] = [
    'title'       => t('wkhtmltopdf (In-process, blocking)'),
    'description' => t('Invokes <code>wkhtmltopdf</code> within the request and blocks further processing of that request until the PDF has been generated.'),
    'class'       => 'views_data_export_pdf_wkhtmltopdf_in_proc_renderer',
    'file'        => 'src/views_data_export_pdf_wkhtmltopdf_in_proc_renderer.inc',
  ];

  if (module_exists('vde_pdf_background_process')) {
    // This renderer wraps the in-proc renderer in a background process renderer
    $renderers['wkhtmltopdf_background_process'] = [
      'title'       => t('wkhtmltopdf (Background process, async)'),
      'description' => t('During batched exports, invokes <code>wkhtmltopdf</code> in an asynchronous background process to avoid load balancer timeouts on large data sets.'),
      'class'       => 'views_data_export_pdf_background_process_renderer',
      'file'        => 'src/views_data_export_pdf_background_process_renderer.inc',
      'file path'   => drupal_get_path('module', 'vde_pdf_background_process'),
      'constructor arguments' => [
        'vde_pdf_wkhtmltopdf',
        'views_data_export_pdf_wkhtmltopdf_in_proc_renderer',
      ]
    ];
  }

  return $renderers;
}

/**
 * Alters the options being passed-in to the active renderer.
 *
 * @param array $options
 *   A reference to the options being passed to the renderer.
 */
function hook_views_data_export_renderer_options_alter(array &$options) {
  // Use the current time and date as the PDF page header.
  $options['header_html'] =
    '<div class="header-left">' . date('c') . '</div>';
}

/**
 * Alters the options being passed-in to mPDF before PDF rendering.
 *
 * @param array $options
 *   A reference to the options being passed to the renderer.
 *   Includes:
 *   - constructor: Options passed to the mPDF constructor:
 *     May include:
 *     - mode: The encoding of text in the file.
 *     - format: An array containing two values that represent the width and
 *       height in millimeters of a page in portrait orientation.
 *     - orientation: Either "P" or "L" for portrait (taller than wide) or
 *       landscape (wider than tall) page orientation, respectively.
 *     - margin_left: The space in millimeters between the left edge of the page
 *       and the left of the content area.
 *     - margin_right: The space in millimeters between the right edge of the
 *       page and the right of the content area.
 *     - margin_top: The space in millimeters between the top edge of the page
 *       and the top of the content area (ignoring headers).
 *     - margin_bottom: The space in millimeters between the bottom edge of the
 *       page and the bottom of the content area (ignoring footers).
 *     - margin_header: The space in millimeters between the top edge of the
 *       page and the top of the header area.
 *     - margin_footer: The space in millimeters between the bottom edge of the
 *       page and the bottom of the footer area.
 *   - additional_options: Options set on mPDF via setter methods.
 *     May include:
 *     - header_html: A fragment of HTML that represents the content that should
 *       appear in the header region of every page.
 *     - footer_html: A fragment of HTML that represents the content that should
 *       appear in the footer region of every page.
 */
function hook_views_data_export_mpdf_options_alter(array &$options) {
  // Change margins
  $new_margins = [
    'margin_left'   => 24,
    'margin_right'  => 24,
    'margin_top'    => 36,
    'margin_bottom' => 36,

    'margin_header' => 18,
    'margin_footer' => 18,
  ];

  $options['constructor'] = array_merge($options['constructor'], $new_margins);
}

/**
 * Alter how mPDF has been configured before PDF rendering.
 *
 * This hook is fired after hook_views_data_export_mpdf_options_alter(), and
 * provides modules with the opportunity to configure additional options on the
 * mPDF object that are not available through renderer optons and/or that might
 * otherwise not be supported by VDE PDF.
 *
 * @param \Mpdf\Mpdf $mPdf
 *   The mPDF render engine instance.
 *
 * @noinspection PhpFullyQualifiedNameUsageInspection
 */
function hook_views_data_export_mpdf_alter(\Mpdf\Mpdf $mPdf) {
  // Set author and creator on PDFs
  $mPdf->SetAuthor("John Hancock");
  $mPdf->SetCreator("Views Data Export PDF");
}

/**
 * Alters the options being passed-in to wkhtmltopdf before PDF rendering.
 *
 * @param array $options
 *   A reference to the options being passed via CLI to the renderer. Contains
 *   a mix of associative (key-value) and ordinal (sequentially numbered)
 *   elements. Can include any of the following, as well as any command-line
 *   argument supported by wkhtmltopdf (see:
 *   https://wkhtmltopdf.org/usage/wkhtmltopdf.txt):
 *   - binary: The path to the wkhtmltopdf executable.
 *   - orientation: Either "portrait" or "landscape" for portrait (taller than
 *     wide) or landscape (wider than tall) page orientation, respectively.
 *   - page-width: The width in millimeters of a page in portrait orientation.
 *   - page-height: The height in millimeters of a page in portrait orientation.
 *   - load-error-handling: How to handle pages that fail to load: "abort",
 *     "ignore", or "skip" (the module defaults to "ignore").
 *   - load-media-error-handling: How to handle media files that fail to load:
 *     "abort", "ignore", or "skip" (the module defaults to "ignore").
 *   - disable-smart-shrinking: An ordinal argument that is usually present to
 *     disable the intelligent shrinking strategy used by WebKit that makes the
 *     pixel/dpi ratio "none" constant, so that page numbering is accurate (as
 *     a workaround for wkhtmltopdf issues #1744 and #1782.
 *   - header-html: A full HTML page that represents the content that should
 *     appear in the header region of every page.
 *   - footer-html: A full HTML page that represents the content that should
 *     appear in the footer region of every page.
 */
function hook_views_data_export_wkhtmltopdf_options_alter(array &$options) {
  // Use the current time and date as the PDF page header.
  $options['header-html'] =
    '<div class="header-left">' . date('c') . '</div>';

  // Workaround for https://github.com/wkhtmltopdf/wkhtmltopdf/issues/1744
  // and https://github.com/wkhtmltopdf/wkhtmltopdf/issues/1782.
  // Disable smart shrinking/auto DPI scaling so that page numbering with
  // wkhtmltopdf is correct.
  $options[] = 'disable-smart-shrinking';
}

/**
 * @} End of "addtogroup hooks".
 */
